public class TreeNode {

   /** Tree
    Node name. */
   private String name;

   /** First child. */
   private TreeNode firstChild;

   /** Next sibling. */
   private TreeNode nextSibling;

   /** Default constructor. */
   public TreeNode() {
   }

   /**
    * Initializes a new node from a given name.
    * @param n a name of the node.
    */
   public TreeNode(String n) {
      name = n;
   }

   /**
    * Initializes a new node.
    * @param n a name of the node.
    * @param d first child node.
    * @param r next sibling node.
    */
   public TreeNode(String n, TreeNode d, TreeNode r) {
      name = n;
      firstChild = d;
      nextSibling = r;
   }

   /**
    * Method to build a tree from the left parenthetic string representation.
    * @param s a string representation of the tree.
    * @return a root node of the tree.
    */
   public static TreeNode parsePrefix(String s) {
      if (s != null && !s.isEmpty()) {
         testString(s);
         TreeNode root = new TreeNode();
         StringBuilder b = new StringBuilder();
         for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') { // expecting children
               root.name = b.toString();
               parse(s.substring(i + 1, s.length() - 1), root, true);
               b.setLength(0);
               break;
            } else // increase name
               b.append(s.charAt(i));
         }
         if (b.length() > 0 && root.firstChild == null)
            root.name = b.toString();
         return root;
      }
      else
         throw new RuntimeException("Can't parse empty string.");
   }


   private static void testString(String s) {

      // Test illegal combination of characters in string
      if (s.contains(",,") || s.contains("(,") || s.contains(",)")) throw new RuntimeException("Illegal string for parsing: " + s);
      if (s.contains("()")) throw new RuntimeException("Empty subtree in string for parsing: " + s);
      if (s.contains("\t")) throw new RuntimeException("Sisaldab TABe: " + s);
      if (s.contains("((")) throw new RuntimeException("Sisaldab topelt ((: " + s);

      for (int i = 0; i <s.length()-1 ; i++) {
         if (Character.isLetter(s.charAt(i))){
            char temp = s.charAt(i+1);
            boolean mid = (temp == ' ');
            if ( mid){
               throw new RuntimeException("Jama valem, topelt sulud, komad alguses: " + s);

            }

         }

      }
      // Test the number of brackets
      int i = s.length() - s.replace("(", "").length();
      int j = s.length() - s.replace(")", "").length();
      if (i != j) throw new RuntimeException("Numbers of brackets do not match: " + s);

      // Test that string starts and end with proper brackets
      for (i=0;i < s.length(); i++) {
         if (s.charAt(i) == '(') {
            break;
         } else if (s.charAt(i) == ')') {
            throw new RuntimeException("Wrong bracket combination: " + s);
         }
      }

      // Test that string has head
      for (i=0;i < s.length(); i++) {
         if (s.charAt(i) == '(') {
            break;
         } else if (s.charAt(i) == ',') {
            throw new RuntimeException("Tree does not have head: " + s);
         }
      }

   }


   /**
    * Helper to parse a tree from string.
    * @param s a string representation of subtree.
    * @param node an active node we are processing.
    * @param root indicates weather this node is parent node.
    * @return cursor position.
    */
   private static int parse(String s, TreeNode node, boolean root) {
      StringBuilder b = new StringBuilder();
      int i = 0;
      while (i < s.length()) {
         switch (s.charAt(i)) {
            case '(':
               if (b.length() > 0) {
                  node = create(node, b.toString(), root);
                  i += (root) ? parse(s.substring(i + 1), node.firstChild, true) : parse(s.substring(i + 1), node.nextSibling, true);
                  if (root)
                     node = node.firstChild;
                  else
                     node = node.nextSibling;
                  b.setLength(0);
               }
               break;
            case ',':
               if (b.length() > 0) {
                  node = create(node, b.toString(), root);
                  i += (root) ? parse(s.substring(i + 1), node.firstChild) : parse(s.substring(i + 1), node.nextSibling);
               } else
                  i += parse(s.substring(i + 1), node);
               return ++i;
            case ')':
               if (b.length() > 0)
                  node = create(node, b.toString(), root);
               return ++i;
            default:
               b.append(s.charAt(i));
               break;
         }
         i++;
      }
      if (b.length() > 0)
         node = create(node, b.toString(), root);
      return i;
   }

   /**
    * Helper to parse a tree from string (in case if not parent node).
    * @param s a string representation of subtree.
    * @param node an active node we are processing.
    * @return cursor position.
    */
   private static int parse(String s, TreeNode node) {
      return parse(s, node, false);
   }

   /**
    * Helper to create appropriate child for the given node.
    * @param node an active node we are processing.
    * @param name a name for the child.
    * @param root indicates weather this node is parent node.
    * @return the node we are processing.
    */
   private static TreeNode create(TreeNode node, String name, boolean root) {
      if (root)
         node.firstChild = new TreeNode(name);
      else
         node.nextSibling = new TreeNode(name);
      return node;
   }

   /**
    * Method to construct the right parenthetic string representation of a tree.
    * @return string representation of the tree.
    */
   public String rightParentheticRepresentation() {
      StringBuilder ret = new StringBuilder();
      TreeNode node = firstChild;
      if (firstChild != null)
         ret.append('(');
      while (node != null) {
         ret.append(node.rightParentheticRepresentation());
         node = node.nextSibling;
         if (node != null)
            ret.append(',');
      }
      if (firstChild != null)
         ret.append(')');
      return ret.append(name).toString();
   }

   /**
    * Main entry point.
    * @param args command line arguments.
    */
   public static void main(String[] args) {
      String s =  "ABC";
      //String s = "+(*(-(2,1),4),/(6,3))";
      // String s = "+(*(-(2,1),4),/(6,3))"; // debug complex tree (expecting "(((2,1)-,4)*,(6,3)/)+")
      TreeNode t = TreeNode.parsePrefix(s);
      String v = t.rightParentheticRepresentation();
      System.out.println(s + " ==> " + v); // A(B,C) ==> (B,C)A
   }

}